import os
import json
import cx_Oracle
import pandas as pd
from tkinter import *


def get_parameters() -> dict:
    """
    Retorna os parametros do arquivo parameters.json
    :return:
    """
    with open(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'parameters.json')) as f:
        parameters = json.loads(f.read())
    return parameters


def get_con_database():
    """
    Retorna o objeto com a conexão para o banco de dados
    :return: sqlalchemy.engine.base.Engine
    """
    curr_path = os.path.dirname(os.path.abspath(__file__))
    with open(os.path.join(curr_path, 'pass.json'), 'r') as f:
        parameters = json.load(f)
    con = cx_Oracle.connect('{}/{}@{}'.format(parameters['prdme.db']['user'],
                                              parameters['prdme.db']['pass'],
                                              parameters['prdme.db']['host']))
    return con


def get_nips_db():
    curr_path = os.path.dirname(os.path.abspath(__file__))
    with open(os.path.join(curr_path, 'pass.json'), 'r') as f:
        parameters = json.load(f)
    with cx_Oracle.connect('{}/{}@{}'.format(parameters['prdme.db']['user'],
                                             parameters['prdme.db']['pass'],
                                             parameters['prdme.db']['host'])) as connection:
        sql = 'SELECT * FROM DBAPS.NIPS_GERENCIAIS'
        df_nips_db = pd.read_sql(sql, connection)
    return df_nips_db



def alert_popup(title, message, path,color, w,h):
    """Generate a pop-up window for special messages."""
    root = Tk()
    root.title(title)
    root.configure(bg=color)
    w = w     # popup window width
    h = h     # popup window height
    sw = root.winfo_screenwidth()
    sh = root.winfo_screenheight()
    x = (sw - w)/2
    y = (sh - h)/2
    root.geometry('%dx%d+%d+%d' % (w, h, x, y))
    m = message
    m += '\n'
    m += path
    w = Label(root, text=m, fg="white",bg=color, width=120, height=10)
    w.pack()
    b = Button(root, text="OK", command=root.destroy, width=10)
    b.pack()
    mainloop()

##alert_popup("Nips", "{} NOVAS NIPS ENCONTRADAS".format("2"), "")


