import pandas as pd
from functions import get_nips_db, alert_popup
from selenium import webdriver
import time
import ctypes


def main():
    try:
        args = ["hide_console", ]
        options = webdriver.ChromeOptions()
        options.add_argument('--headless')
        options.add_experimental_option('excludeSwitches', ['enable-logging'])

        ## faz a comunicação do selenium com o Google Chrome
        driver = webdriver.Chrome('C:/chromedriver.exe', service_args=args, options=options)

        ## executa a url
        driver.get('https://www2.ans.gov.br/ans-idp/')
        time.sleep(2)
        ## pega o campo input usuário e senha
        usuario = driver.find_element_by_id('input-mask')
        senha = driver.find_element_by_id('mod-login-password')
        time.sleep(2)
        # passa no input os valores de login e senha
        usuario.send_keys('39840562819')
        time.sleep(2)
        senha.send_keys('bSPTBT@354')
        time.sleep(2)

        # executa o botão acessar
        driver.find_element_by_id('botao').click()
        time.sleep(1)
        url = driver.find_element_by_xpath("//a[@class='ui-menuitem-link ui-corner-all']").get_attribute('href')
        driver.get(url)

        table = driver.find_element_by_xpath(
            "//*[@id='formContent:j_idt85:tbDemandaAguardandoResposta_data']//ancestor::table[1]")

        table_html = table.get_attribute('outerHTML')
        df = pd.read_html(str(table_html), header=0)[0]
        df['Demanda'] = df['Demanda'].apply(lambda x: str(x))

        print('Pegando NIPS do Banco de dados')
        df_nips_db = get_nips_db()

        texto = 'NIPs Encontradas:\n'
        for i, r in df.iterrows():

            if (r['Demanda']) in df_nips_db['DEMANDA'].values:
                print('Número demanda: ' + (r['Demanda']) + ' já esta cadastrado')

            if (r['Demanda']) not in df_nips_db['DEMANDA'].values:
                print('Número demanda: ' + (r['Demanda']) + ' não esta cadastrado')
                texto = texto + 'N° {} - {} - {}\n'.format(r['Demanda'], (
                        r['Beneficiário'].split()[0] + ' ' + r['Beneficiário'].split()[1]), r['Natureza'])

        if texto == 'NIPs Encontradas:\n':
            alert_popup("Novas Nips", 'Nenhuma Nip encontrada', "", color='blue', w=300, h=180)
        else:
            alert_popup("Novas Nips", texto, "", color='red', w=310, h=180)


    except Exception as e:
        alert_popup("Novas Nips", "Erro, favor reportar ao administrador", "", color='red', w=250, h=150)
        print(e)

    # if cont > 0:
    #     if cont > 1:
    #         alert_popup("Novas Nips", "{} NOVAS NIPS ENCOTRADAS".format(cont), "")
    #     else:
    #         alert_popup("Novas Nips", "{} NOVA NIP ENCOTRADA".format(cont), "")
    # else:
    #     ctypes.windll.user32.MessageBoxW(0, "Não há uma nova NIP", "Novas Nips", 1)

    driver.close()


# df_Colaboradores = pd.read_excel('N:/TI/FUNCIONARIOS/Christian/Colaboradores 30.10.2020.xlsx')
# cxfreeze main.py --target-dir ./ para gerar o executavel


if __name__ == '__main__':
    main()
    # Schedule.cada.tempo.fazer
    # schedule.every(20).minutes.do(main)
    # while 1:
    #     schedule.run_pending()
    #     time.sleep(1)
else:
    print('Script só executa pelo terminal e não como modulo')
